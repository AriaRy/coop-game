// Fill out your copyright notice in the Description page of Project Settings.

#pragma warning (disable : 4706)

#include "Components/SHealthComponent.h"

#include "SGameMode.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
USHealthComponent::USHealthComponent()
{
	DefaultHealth = 100;

	TeamNumber = 255;

	// SetIsReplicated(true);
	SetIsReplicatedByDefault(true);
}


// Called when the game starts
void USHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// Only hook if we are server
	if (GetOwnerRole() == ROLE_Authority)
	{
		AActor* MyOwner = GetOwner();
		if (MyOwner)
			MyOwner->OnTakeAnyDamage.AddDynamic(this, &USHealthComponent::HandleTakeAnyDamage);
	}

	Health = DefaultHealth;
}

void USHealthComponent::OnRep_Health(const float OldHealth) const
{
	const float Damage = OldHealth - Health;
	OnHealthChanged.Broadcast(this, Health, Damage, nullptr, nullptr, nullptr);
}

void USHealthComponent::HandleTakeAnyDamage(AActor* DamagedActor, const float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (bIsDead || Damage <= 0.0f) return;

	if (DamagedActor != DamageCauser && IsFriendly(DamagedActor, DamageCauser)) return;

	// Update health clamped
	Health = FMath::Clamp(Health - Damage, 0.0f, DefaultHealth);

	OnHealthChanged.Broadcast(this, Health, Damage, DamageType, InstigatedBy, DamageCauser);

	if (bIsDead = Health <= 0.0f)
	{
		ASGameMode* GameMode = Cast<ASGameMode>(GetWorld()->GetAuthGameMode());
		if (GameMode)
			GameMode->OnActorKilled.Broadcast(DamagedActor, DamageCauser, InstigatedBy);
	}
}

void USHealthComponent::Heal(float Amount)
{
	if (Health <= 0.0f) return;

	Health = FMath::Clamp(Health + Amount, 0.0f, DefaultHealth);

	OnHealthChanged.Broadcast(this, Health, -Amount, nullptr, nullptr, nullptr);
}

bool USHealthComponent::IsFriendly(AActor* A, AActor* B)
{
	if (!A || !B) return true;

	USHealthComponent* HealthComponentA = Cast<USHealthComponent>(A->GetComponentByClass(USHealthComponent::StaticClass()));
	USHealthComponent* HealthComponentB = Cast<USHealthComponent>(B->GetComponentByClass(USHealthComponent::StaticClass()));

	if (!HealthComponentA || !HealthComponentB) return true;

	return HealthComponentA->TeamNumber == HealthComponentB->TeamNumber;
}

void USHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(USHealthComponent, Health);
}
