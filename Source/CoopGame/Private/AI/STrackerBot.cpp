// Fill out your copyright notice in the Description page of Project Settings.

#pragma warning (disable : 4458)

#include "AI/STrackerBot.h"

#include "DrawDebugHelpers.h"
#include "EngineUtils.h"
#include "NavigationPath.h"
#include "NavigationSystem.h"
#include "SCharacter.h"
#include "Components/SHealthComponent.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

// Sets default values
ASTrackerBot::ASTrackerBot()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMesh->SetCanEverAffectNavigation(false);
	StaticMesh->SetSimulatePhysics(true);
	RootComponent = StaticMesh;

	HealthComponent = CreateDefaultSubobject<USHealthComponent>("HealthComponent");
	HealthComponent->OnHealthChanged.AddDynamic(this, &ASTrackerBot::HandleTakeDamage);

	SphereComponent = CreateDefaultSubobject<USphereComponent>("SphereComponent");
	SphereComponent->SetSphereRadius(200);
	SphereComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SphereComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	SphereComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	SphereComponent->SetupAttachment(RootComponent);

	MovementForce = 500.0f;
	bUseVelocityChange = true;
	RequiredDistanceToTarget = 100.0f;

	ExplosionDamage = 40.0f;
	ExplosionRadius = 300.0f;

	SelfDamageInterval = 0.25f;
}

// Called when the game starts or when spawned
void ASTrackerBot::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority())
		NextPathPoint = GetNextPathPoint();

	FTimerHandle TimerHandle_CheckPowerLevel;
	GetWorldTimerManager().SetTimer(TimerHandle_CheckPowerLevel, this, &ASTrackerBot::OnCheckNearbyBots, 1.0f, true);

}

void ASTrackerBot::HandleTakeDamage(const USHealthComponent* OwningHealthComponent, float Health, const float HealthDelta, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (!MaterialInstance)
		MaterialInstance = StaticMesh->CreateAndSetMaterialInstanceDynamicFromMaterial(0, StaticMesh->GetMaterial(0));
	else
		MaterialInstance->SetScalarParameterValue("LastTimeDamageTaken", GetWorld()->TimeSeconds);

	if (Health <= 0)
		SelfDestruct();
}

FVector ASTrackerBot::GetNextPathPoint()
{
	AActor* BestTarget = nullptr;
	float NearestTargetDistance = FLT_MAX;
	for (TActorIterator<ASCharacter> It(GetWorld()); It; ++It)
	{
		APawn* Pawn = *It;
		if (Pawn && !USHealthComponent::IsFriendly(this, Pawn))
		{
			USHealthComponent* HealthComponent = Cast<USHealthComponent>(Pawn->GetComponentByClass(USHealthComponent::StaticClass()));
			if (HealthComponent && HealthComponent->GetHealth() > 0.0f)
			{
				const float Distance = GetDistanceTo(Pawn);

				if (Distance < NearestTargetDistance)
				{
					NearestTargetDistance = Distance;
					BestTarget = Pawn;
				}
			}
		}
	}

	if (BestTarget)
	{
		UNavigationPath* NavPath = UNavigationSystemV1::FindPathToActorSynchronously(this, GetActorLocation(), BestTarget);

		GetWorldTimerManager().ClearTimer(TimerHandle_RefreshPath);
		GetWorldTimerManager().SetTimer(TimerHandle_RefreshPath, this, &ASTrackerBot::RefreshPath, 5.0f, false);

		if (NavPath && NavPath->PathPoints.Num() > 1)
			return NavPath->PathPoints[1];
	}

	return GetActorLocation();
}

void ASTrackerBot::SelfDestruct()
{
	if (bExploded) return;

	bExploded = true;

	// if (ExplosionEffect)
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionEffect, GetActorLocation());

	UGameplayStatics::PlaySoundAtLocation(this, ExplosionSound, GetActorLocation());

	StaticMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	StaticMesh->SetVisibility(false, true);

	if (HasAuthority())
	{
		const float ActualDamage = ExplosionDamage + (ExplosionDamage * PowerLevel);
		UGameplayStatics::ApplyRadialDamage(this, ActualDamage, GetActorLocation(), ExplosionRadius, DamageType, TArray<AActor*>(), this, GetInstigatorController(), true);

		// Destroy();
		SetLifeSpan(2.0f);
	}
}

void ASTrackerBot::DamageSelf()
{
	UGameplayStatics::ApplyDamage(this, 20, GetInstigatorController(), this, nullptr);
}

void ASTrackerBot::OnCheckNearbyBots()
{
	const float Radius = 600;

	const FCollisionShape CollisionShape = FCollisionShape::MakeSphere(Radius);

	FCollisionObjectQueryParams QueryParams = FCollisionObjectQueryParams(ECC_Pawn);
	QueryParams.AddObjectTypesToQuery(ECC_PhysicsBody);

	TArray<FOverlapResult> OverlapResult;
	GetWorld()->OverlapMultiByObjectType(OverlapResult, GetActorLocation(), FQuat::Identity, QueryParams, CollisionShape);

	int32 NumberOfBots = 0;
	for (FOverlapResult Result : OverlapResult)
	{
		ASTrackerBot* Bot = Cast<ASTrackerBot>(Result.GetActor());
		if (Bot && Bot != this)
			NumberOfBots++;
	}

	const int32 MaxPowerLevel = 4;

	PowerLevel = FMath::Clamp(NumberOfBots, 0, MaxPowerLevel);

	if (!MaterialInstance)
		MaterialInstance = StaticMesh->CreateAndSetMaterialInstanceDynamicFromMaterial(0, StaticMesh->GetMaterial(0));
	else
	{
		const float Alpha = PowerLevel / static_cast<float>(MaxPowerLevel);
		MaterialInstance->SetScalarParameterValue("PowerLevelAlpha", Alpha);
	}
}

void ASTrackerBot::RefreshPath()
{
	NextPathPoint = GetNextPathPoint();
}

// Called every frame
void ASTrackerBot::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (HasAuthority() && !bExploded)
	{
		const float DistanceToTarget = (GetActorLocation() - NextPathPoint).Size();

		if (DistanceToTarget <= RequiredDistanceToTarget)
			NextPathPoint = GetNextPathPoint();
		else
		{
			FVector ForceDirection = NextPathPoint - GetActorLocation();
			ForceDirection.Normalize();
			ForceDirection *= MovementForce;
			StaticMesh->AddForce(ForceDirection, NAME_None, bUseVelocityChange);
		}
	}
}

void ASTrackerBot::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	if (!bStartedSelfDestruction && !bExploded)
	{
		const ASCharacter* PlayerPawn = Cast<ASCharacter>(OtherActor);
		if (PlayerPawn && !USHealthComponent::IsFriendly(this, OtherActor))
		{
			if (HasAuthority())
				GetWorldTimerManager().SetTimer(TimerHandle_SelfDamage, this, &ASTrackerBot::DamageSelf, SelfDamageInterval, true, 0.0f);

			bStartedSelfDestruction = true;

			UGameplayStatics::SpawnSoundAttached(SelfDestructSound, RootComponent);
		}
	}
}
