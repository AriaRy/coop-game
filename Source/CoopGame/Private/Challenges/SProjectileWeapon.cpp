// Fill out your copyright notice in the Description page of Project Settings.


#include "Challenges/SProjectileWeapon.h"

#include "Challenges/SGrenadeProjectile.h"

void ASProjectileWeapon::Fire()
{
	AActor* MyOwner = GetOwner();
	if (MyOwner && ProjectileClass)
	{
		FVector EyeLocation;
		FRotator EyeRotation;
		MyOwner->GetActorEyesViewPoint(EyeLocation, EyeRotation);

		const FVector MuzzleLocation = MeshComponent->GetSocketLocation(MuzzleSocketName);

		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		ASGrenadeProjectile* Projectile = Cast<ASGrenadeProjectile>(GetWorld()->SpawnActor<ASGrenadeProjectile>(ProjectileClass, MuzzleLocation, EyeRotation, SpawnParameters));
		if (Projectile)
		{
			Projectile->SetOwner(this);
		}

		LastFiredTime = GetWorld()->TimeSeconds;
	}
}
