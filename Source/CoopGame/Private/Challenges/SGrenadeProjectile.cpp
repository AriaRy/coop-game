// Fill out your copyright notice in the Description page of Project Settings.


#include "Challenges/SGrenadeProjectile.h"

#include "DrawDebugHelpers.h"
#include "SCharacter.h"
#include "SWeapon.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"

static int32 DebugExplosionDrawing = 0;
FAutoConsoleVariableRef CVarDebugExplosionDrawing(
	TEXT("COOP.DebugExplosions"),
	DebugExplosionDrawing,
	TEXT("Draw Debug Spheres for Explosions"),
	ECVF_Cheat);

// Sets default values
ASGrenadeProjectile::ASGrenadeProjectile()
{
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("StaticMeshComponent");
	StaticMeshComponent->SetSimulatePhysics(true);
	RootComponent = StaticMeshComponent;

	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>("ProjectileMovementComponent");
	ProjectileMovementComponent->InitialSpeed = 2000;
	ProjectileMovementComponent->MaxSpeed = 2000;
	ProjectileMovementComponent->bShouldBounce = true;

	ExplosionTime = 2.0f;
	ExplosionRadius = 400.0f;
	ExplosionScale = FVector::OneVector;
}

void ASGrenadeProjectile::BeginPlay()
{
	GetWorldTimerManager().SetTimer(TimerHandle, this, &ASGrenadeProjectile::Explode, ExplosionTime);
}

void ASGrenadeProjectile::Explode()
{
	if (ExplosionEffect)
	{
		const FTransform EmitterTransform = FTransform(FRotator::ZeroRotator, GetActorLocation(), ExplosionScale);

		// Play explosion effect
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionEffect, EmitterTransform);
	}

	ASWeapon* MyOwner = Cast<ASWeapon>(GetOwner());
	if (MyOwner)
	{
		APlayerController* PlayerController = Cast<APlayerController>(MyOwner->GetInstigatorController());

		if (PlayerController)
		{
			const float BaseDamage = MyOwner->GetBaseDamage();

			const TArray<AActor*> IgnoredActors = {this};

			// FIXME: Doesn't damage TargetDummy
			// Do damage to players in range
			const bool DidDamage = UGameplayStatics::ApplyRadialDamage(GetWorld(), BaseDamage, GetActorLocation(), ExplosionRadius, DamageType, IgnoredActors, MyOwner, PlayerController);

			// DEBUG CONTENT
			if (DebugExplosionDrawing > 0)
			{
				DrawDebugSphere(GetWorld(), GetActorLocation(), ExplosionRadius, 12.0f, FColor::Red, false, 3.0f, 0, 1.0f);
				UE_LOG(LogTemp, Log, TEXT("Did Damage: %d"), DidDamage);
			}
		}
	}

	// Pawn object types
	const TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes = {UEngineTypes::ConvertToObjectType(ECC_Pawn)};

	// Filters only SCharacter (Players)
	UClass* ActorClassFilter = ASCharacter::StaticClass();

	// Result
	TArray<AActor*> OutActors;

	const bool Overlapped = UKismetSystemLibrary::SphereOverlapActors(GetWorld(), GetActorLocation(), ExplosionRadius, ObjectTypes, ActorClassFilter, TArray<AActor*>(), OutActors);

	if (Overlapped)
	{
		for (auto OutActor : OutActors)
		{
			APlayerController* PlayerController = Cast<APlayerController>(Cast<ASCharacter>(OutActor)->GetController());
			if (PlayerController) // Always must be true
			{
				// Shakes nearby players cameras when explodes
				PlayerController->ClientPlayCameraShake(ExplosionCameraShake);
			}
		}
	}
	// FIXME: Doesn't Work
	UE_LOG(LogTemp, Log, TEXT("Destroyed: %d"), Destroy());
}
