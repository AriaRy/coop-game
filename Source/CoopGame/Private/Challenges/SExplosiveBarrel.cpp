// Fill out your copyright notice in the Description page of Project Settings.

#pragma warning (disable : 4458)

#include "Challenges/SExplosiveBarrel.h"

#include "Components/SHealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "PhysicsEngine/RadialForceComponent.h"

// Sets default values
ASExplosiveBarrel::ASExplosiveBarrel()
{
 	HealthComponent = CreateDefaultSubobject<USHealthComponent>("HealthComponent");
	HealthComponent->OnHealthChanged.AddDynamic(this, &ASExplosiveBarrel::OnHealthChanged);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMesh->SetSimulatePhysics(true);
	StaticMesh->SetCollisionObjectType(ECC_PhysicsBody);
	RootComponent = StaticMesh;

	RadialForceComponent = CreateDefaultSubobject<URadialForceComponent>("RadialForceComponent");
	RadialForceComponent->SetupAttachment(StaticMesh);
	RadialForceComponent->Radius = 250;
	RadialForceComponent->bImpulseVelChange = true;
	RadialForceComponent->bAutoActivate = false;
	RadialForceComponent->bIgnoreOwningActor = true;

	ExplosionImpulse = 700;
	ExplosionDamage = 70.0f;
	ExplosionRadius = 400.0f;

	SetReplicates(true);
    SetReplicateMovement(true);

}

void ASExplosiveBarrel::OnHealthChanged(const USHealthComponent* OwningHealthComponent, float Health, const float HealthDelta, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (bExploded) return;

	if (Health <= 0.0f)
	{
		bExploded = true;
		OnRep_bExploded();

		const FVector BoostIntensity = FVector::UpVector * ExplosionImpulse;
		StaticMesh->AddImpulse(BoostIntensity, NAME_None, true);

		RadialForceComponent->FireImpulse();

		// FIXME: Crashes when damage a BP_Character
		UGameplayStatics::ApplyRadialDamage(GetWorld(), ExplosionDamage, GetActorLocation(), ExplosionRadius, this->DamageType, TArray<AActor*>(), this, InstigatedBy);
	}
}

void ASExplosiveBarrel::OnRep_bExploded()
{
	if (ExplosionEffect)
	{
		// Play FX
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionEffect, GetActorLocation());
	}

	if (ExplodedMaterial)
	{
		// Change material to exploded
		StaticMesh->SetMaterial(0, ExplodedMaterial);
	}
}

void ASExplosiveBarrel::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASExplosiveBarrel, bExploded);
}