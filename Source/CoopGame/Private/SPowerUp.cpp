// Fill out your copyright notice in the Description page of Project Settings.


#include "SPowerUp.h"

#include "Net/UnrealNetwork.h"

// Sets default values
ASPowerUp::ASPowerUp()
{
	RotatingMovement = CreateDefaultSubobject<URotatingMovementComponent>("RotatingMovement");

	PowerUpInterval = 0.0f;
	NumberOfTicks = 0;

	SetReplicates(true);
}

void ASPowerUp::OnTickPowerUp()
{
	OnPowerUpTicked();

	if (TicksProcessed >= NumberOfTicks)
	{
		bIsPowerUpActive = false;
		OnRep_bIsPowerUpActive();

		OnExpired();
		GetWorldTimerManager().ClearTimer(TimerHandle_PowerUpTick);
	}

	TicksProcessed++;
}

void ASPowerUp::OnRep_bIsPowerUpActive()
{
	OnPowerUpStateChanged(bIsPowerUpActive);
}

void ASPowerUp::ActivatePowerUp(AActor* Activator)
{
	bIsPowerUpActive = true;
	OnRep_bIsPowerUpActive();

	if (PowerUpInterval)
		GetWorldTimerManager().SetTimer(TimerHandle_PowerUpTick, this, &ASPowerUp::OnTickPowerUp, PowerUpInterval, true);
	else
		OnTickPowerUp();

	OnActivated(Activator);
}

void ASPowerUp::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASPowerUp, bIsPowerUpActive);
}