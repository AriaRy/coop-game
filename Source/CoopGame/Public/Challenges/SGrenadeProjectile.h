// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SGrenadeProjectile.generated.h"

class UStaticMeshComponent;
class UProjectileMovementComponent;

UCLASS()
class COOPGAME_API ASGrenadeProjectile : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASGrenadeProjectile();

protected:

	virtual void BeginPlay() override;;

	UPROPERTY(EditDefaultsOnly, Category = "Components")
	UStaticMeshComponent* StaticMeshComponent;

	UPROPERTY(EditDefaultsOnly, Category = "Components")
	UProjectileMovementComponent* ProjectileMovementComponent;

	UFUNCTION()
	void Explode();

	FTimerHandle TimerHandle;

	UPROPERTY(EditDefaultsOnly, Category = "Grenade")
	UParticleSystem* ExplosionEffect;

	UPROPERTY(EditDefaultsOnly, Category = "Grenade")
	float ExplosionTime;

	UPROPERTY(EditDefaultsOnly, Category = "Grenade")
	float ExplosionRadius;

	UPROPERTY(EditDefaultsOnly, Category = "Grenade")
	FVector ExplosionScale;

	UPROPERTY(EditDefaultsOnly, Category = "Grenade")
	TSubclassOf<UDamageType> DamageType;

	UPROPERTY(EditDefaultsOnly, Category = "Grenade")
	TSubclassOf<UCameraShake> ExplosionCameraShake;
};
