// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SExplosiveBarrel.generated.h"

class URadialForceComponent;
class USHealthComponent;

UCLASS()
class COOPGAME_API ASExplosiveBarrel : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASExplosiveBarrel();

protected:

	UPROPERTY(VisibleAnywhere, Category = "Components")
	USHealthComponent* HealthComponent;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	URadialForceComponent* RadialForceComponent;

	UFUNCTION()
	void OnHealthChanged(const class USHealthComponent* OwningHealthComponent, float Health, const float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	UPROPERTY(ReplicatedUsing = OnRep_bExploded)
	bool bExploded;

	UFUNCTION()
	void OnRep_bExploded();

	UPROPERTY(EditAnywhere, Category = "FX")
	float ExplosionImpulse;

	UPROPERTY(EditDefaultsOnly, Category ="FX")
	UParticleSystem* ExplosionEffect;

	UPROPERTY(EditDefaultsOnly, Category = "FX")
	UMaterialInterface* ExplodedMaterial;

	UPROPERTY(EditAnywhere, Category = "FX")
	float ExplosionDamage;

	UPROPERTY(EditAnywhere, Category = "FX")
	float ExplosionRadius;

	UPROPERTY(EditDefaultsOnly, Category = "FX")
	TSubclassOf<UDamageType> DamageType;
};
