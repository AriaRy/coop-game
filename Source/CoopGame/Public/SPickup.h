// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SPickup.generated.h"

class ASPowerUp;
class USphereComponent;

UCLASS()
class COOPGAME_API ASPickup : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASPickup();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	USphereComponent* SphereComponent;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UDecalComponent* DecalComponent;

	UPROPERTY(EditInstanceOnly, Category = "PickUp")
	TSubclassOf<ASPowerUp> PowerUpClass;

	void Respawn();

	ASPowerUp* PowerUpInstance;

	UPROPERTY(EditInstanceOnly, Category = "PickUp")
	float CoolDownDuration;

	FTimerHandle TimerHandle_Respawn;

public:

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

};
