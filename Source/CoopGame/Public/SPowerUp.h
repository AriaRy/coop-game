// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFramework/RotatingMovementComponent.h"

#include "SPowerUp.generated.h"

UCLASS()
class COOPGAME_API ASPowerUp : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASPowerUp();

protected:

	// Time between power up ticks
	UPROPERTY(EditDefaultsOnly, Category = "PowerUps")
	float PowerUpInterval;

	// Total times we apply the power up effect
	UPROPERTY(EditDefaultsOnly, Category = "PowerUps")
	int32 NumberOfTicks;

	FTimerHandle TimerHandle_PowerUpTick;

	int32 TicksProcessed;

	UFUNCTION()
	void OnTickPowerUp();

	UPROPERTY(EditDefaultsOnly, Category = "Components")
	URotatingMovementComponent* RotatingMovement;

	UPROPERTY(ReplicatedUsing = OnRep_bIsPowerUpActive)
	bool bIsPowerUpActive;

	UFUNCTION()
	void OnRep_bIsPowerUpActive();

	UFUNCTION(BlueprintImplementableEvent, Category = "PowerUps")
	void OnPowerUpStateChanged(bool bNewIsActive);

public:

	void ActivatePowerUp(AActor* Activator);

	UFUNCTION(BlueprintImplementableEvent, Category = "PowerUps")
	void OnActivated(AActor* Activator);

	UFUNCTION(BlueprintImplementableEvent, Category = "PowerUps")
	void OnExpired();

	UFUNCTION(BlueprintImplementableEvent, Category = "PowerUps")
	void OnPowerUpTicked();
};
