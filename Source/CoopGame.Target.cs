// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class CoopGameTarget : TargetRules
{
    public CoopGameTarget(TargetInfo Target) : base(Target)
    {
        Type = TargetType.Game;
        DefaultBuildSettings = BuildSettingsVersion.V2;
        ExtraModuleNames.AddRange(new[] {"CoopGame"});
    }
}